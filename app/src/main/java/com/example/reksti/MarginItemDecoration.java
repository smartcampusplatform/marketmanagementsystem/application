package com.example.reksti;

import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class MarginItemDecoration extends RecyclerView.ItemDecoration {

    private int marginCardVIew;
    private boolean isHorizontal;

    public MarginItemDecoration(int marginCardVIew, boolean isHorizontal) {
        this.marginCardVIew = marginCardVIew;
        this.isHorizontal = isHorizontal;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        if (isHorizontal) {
            if (parent.getChildAdapterPosition(view) == 0) {
                outRect.left = marginCardVIew;
            }

            outRect.right = marginCardVIew;
        } else {
            if (parent.getChildAdapterPosition(view) == 0) {
                outRect.top = marginCardVIew;
            }

            outRect.bottom = marginCardVIew;
        }
    }
}
