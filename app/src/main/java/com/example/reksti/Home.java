package com.example.reksti;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class Home extends Fragment {

    private ArrayList<String> favoriteItem_mImageUrls = new ArrayList<>();
    private ArrayList<String> favoriteItem_mNames = new ArrayList<>();
    private ArrayList<String> favoriteItem_mLocs = new ArrayList<>();

    private ArrayList<String> itemCategory_mImageUrls = new ArrayList<>();
    private ArrayList<String> itemCategory_mNames = new ArrayList<>();

    public Home() {
        // Required empty public constructor
    }

    private void getImages_favoriteItem() {
        favoriteItem_mImageUrls.add("https://c1.staticflickr.com/5/4636/25316407448_de5fbf183d_o.jpg");
        favoriteItem_mNames.add("Merchant");
        favoriteItem_mLocs.add("Location");

        favoriteItem_mImageUrls.add("https://i.redd.it/tpsnoz5bzo501.jpg");
        favoriteItem_mNames.add("Merchant");
        favoriteItem_mLocs.add("Location");

        favoriteItem_mImageUrls.add("https://i.redd.it/qn7f9oqu7o501.jpg");
        favoriteItem_mNames.add("Merchant");
        favoriteItem_mLocs.add("Location");

        favoriteItem_mImageUrls.add("https://i.redd.it/j6myfqglup501.jpg");
        favoriteItem_mNames.add("Merchant");
        favoriteItem_mLocs.add("Location");

        favoriteItem_mImageUrls.add("https://i.redd.it/0h2gm1ix6p501.jpg");
        favoriteItem_mNames.add("Merchant");
        favoriteItem_mLocs.add("Location");

        favoriteItem_mImageUrls.add("https://i.redd.it/k98uzl68eh501.jpg");
        favoriteItem_mNames.add("Merchant");
        favoriteItem_mLocs.add("Location");

        initRecyclerView_favoriteItem();
    }

    private void getImages_itemCategory() {
        itemCategory_mImageUrls.add("https://i.redd.it/glin0nwndo501.jpg");
        itemCategory_mNames.add("Kantin");

        itemCategory_mImageUrls.add("https://i.redd.it/obx4zydshg601.jpg");
        itemCategory_mNames.add("Fotokopi");

        itemCategory_mImageUrls.add("https://i.imgur.com/ZcLLrkY.jpg");
        itemCategory_mNames.add("Koperasi");

        initRecyclerView_itemCategory();
    }

    private void initRecyclerView_favoriteItem() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);

        RecyclerView recyclerView = (RecyclerView) getActivity().findViewById(R.id.scrollView_favoriteItem);
        recyclerView.setLayoutManager(layoutManager);

        FavoriteItemAdapter adapter = new FavoriteItemAdapter(favoriteItem_mImageUrls, favoriteItem_mNames, favoriteItem_mLocs, getContext());
        recyclerView.setAdapter(adapter);

        Resources resources = getResources();
        recyclerView.addItemDecoration(new MarginItemDecoration((int)resources.getDimension(R.dimen.twice_default_gap), true));
    }

    private void initRecyclerView_itemCategory() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);

        RecyclerView recyclerView = (RecyclerView) getActivity().findViewById(R.id.scrollView_itemCategory);
        recyclerView.setLayoutManager(layoutManager);

        ItemCategoryAdapter adapter = new ItemCategoryAdapter(itemCategory_mImageUrls, itemCategory_mNames, getContext());
        recyclerView.setAdapter(adapter);

        Resources resources = getResources();
        recyclerView.addItemDecoration(new MarginItemDecoration((int)resources.getDimension(R.dimen.twice_default_gap), true));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getImages_favoriteItem();
        getImages_itemCategory();

    }
}
