package com.example.reksti;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class Order extends Fragment {

    private ArrayList<String> recentOrder_mImageUrls = new ArrayList<>();
    private ArrayList<String> recentOrder_mNames = new ArrayList<>();

    public Order() {
        // Required empty public constructor
    }

    private void getImages_recentOrder() {
        recentOrder_mImageUrls.add("https://c1.staticflickr.com/5/4636/25316407448_de5fbf183d_o.jpg");
        recentOrder_mNames.add("Merchant");

        recentOrder_mImageUrls.add("https://i.redd.it/tpsnoz5bzo501.jpg");
        recentOrder_mNames.add("Merchant");

        recentOrder_mImageUrls.add("https://i.redd.it/qn7f9oqu7o501.jpg");
        recentOrder_mNames.add("Merchant");

        recentOrder_mImageUrls.add("https://i.redd.it/j6myfqglup501.jpg");
        recentOrder_mNames.add("Merchant");

        recentOrder_mImageUrls.add("https://i.redd.it/0h2gm1ix6p501.jpg");
        recentOrder_mNames.add("Merchant");

        recentOrder_mImageUrls.add("https://i.redd.it/k98uzl68eh501.jpg");
        recentOrder_mNames.add("Merchant");

        initRecyclerView_recentOrder();
    }

    private void initRecyclerView_recentOrder() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        RecyclerView recyclerView = (RecyclerView) getActivity().findViewById(R.id.scrollView_recentOrder);
        recyclerView.setLayoutManager(layoutManager);

        RecentOrderAdapter adapter = new RecentOrderAdapter(recentOrder_mImageUrls, recentOrder_mNames, getContext());
        recyclerView.setAdapter(adapter);

        Resources resources = getResources();
        recyclerView.addItemDecoration(new MarginItemDecoration((int)resources.getDimension(R.dimen.twice_default_gap), false));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_order, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getImages_recentOrder();
    }
}
